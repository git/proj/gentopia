# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit gnome2 eutils

DESCRIPTION="NetworkManager vpnc daemon/client configuration and management in an easy way."
HOMEPAGE="http://people.redhat.com/dcbw/NetworkManager/"
SRC_URI="http://steev.net/files/distfiles/NetworkManager-vpnc-${PV}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE="crypt doc gnome"

RDEPEND=">=sys-apps/dbus-0.35.2
	>=sys-apps/hal-0.5
	sys-apps/iproute2
	>=net-misc/dhcdbd-1.4
	>=net-wireless/wireless-tools-28_pre9
	>=dev-libs/glib-2.8
	>=net-misc/networkmanager-0.5.1
	>=net-misc/vpnc-0.3.3
	gnome? ( >=x11-libs/gtk+-2.8
		>=gnome-base/libglade-2
		>=gnome-base/gnome-keyring-0.4
		>=gnome-base/gnome-panel-2
		>=gnome-base/gconf-2
		>=gnome-base/libgnomeui-2 )
	crypt? ( dev-libs/libgcrypt )"
	
DEPEND="${RDEPEND}
	dev-util/pkgconfig
	dev-util/intltool"

S=${WORKDIR}/NetworkManager-vpnc-${PV}

DOCS="AUTHORS COPYING ChangeLog INSTALL NEWS README"
USE_DESTDIR="1"

G2CONF="${G2CONF} \
	`use_with crypt gcrypt` \
	--disable-more-warnings \
	--with-dbus-sys=/etc/dbus-1/system.d \
	--enable-notification-icon"

src_unpack () {

	unpack ${A}
	cd ${S}
	# Gentoo puts vpnc somewhere that the source doesn't expect.
	epatch ${FILESDIR}/nm-vpnc-path.patch
	# Match the same dbus permissions as NetworkManager
	epatch ${FILESDIR}/nm-vpnc-dbus_conf.patch
}

#pkg_postinst() {
#	einfo "NetworkManager depends on the at_console property of DBUS"
#	einfo "to give permission to configure the networks.  This means"
#	einfo "you either need pam_console configured (see"
#	einfo "/usr/portage/sys-libs/pam/files/README.pam_console)"
#	einfo "or you need to touch /var/run/console/\$USERNAME for the user"
#	einfo "logging in at the console, and remove it when they log back"
#	einfo "out."
#}
