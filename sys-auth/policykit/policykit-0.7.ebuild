# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/sys-auth/consolekit/consolekit-0.2.1.ebuild,v 1.13 2007/08/28 19:25:58 jer Exp $

inherit autotools bash-completion eutils multilib

MY_PN="PolicyKit"

DESCRIPTION="Policy framework for setting user allowed actions with priviledge"
HOMEPAGE="http://hal.freedesktop.org/docs/PolicyKit"
SRC_URI="http://hal.freedesktop.org/releases/${MY_PN}-${PV}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="bash-completion doc pam selinux zsh-completion"

RDEPEND=">=dev-libs/glib-2.6
		 >=dev-libs/dbus-glib-0.73
		 dev-libs/expat
		 pam? ( virtual/pam )
		 selinux? ( sys-libs/libselinux )"
DEPEND="${RDEPEND}
		dev-libs/libxslt
		app-text/docbook-xsl-stylesheets
		>=dev-util/pkgconfig-0.18
		>=dev-util/intltool-0.36
		doc? ( dev-util/gtk-doc )"

S="${WORKDIR}/${MY_PN}-${PV}"

pkg_setup() {
	enewgroup polkituser || die "failed to create group"
	enewuser polkituser -1 "-1" /dev/null polkituser || die "failed to create user"
}

src_unpack() {
	unpack ${A}
	cd "${S}"

	epatch "${FILESDIR}/${PN}-0.7-completions.patch"
	eautoreconf
}

src_compile() {
	local authdb=

	if use pam ; then
		authdb="--with-authdb=default --with-pam-module-dir=/$(get_libdir)/security"
	else
		authdb="--with-authdb=dummy"
	fi

	econf ${authdb} \
		  --without-bash-completion \
		  --without-zsh-completion \
		  --enable-man-pages \
		  --with-os-type=gentoo \
		  --with-polkit-user=polkituser \
		  --with-polkit-group=polkituser \
		  $(use_enable selinux) \
		  --localstatedir=/var \
	|| die "configure failed"
	emake || die "emake failed"
}

src_install() {
	emake DESTDIR="${D}" install || die "emake install failed"

	dobashcompletion "${S}/tools/polkit-bash-completion.sh"

	if use zsh-completion ; then
		insinto /usr/share/zsh/site-functions
		doins "${S}/tools/_polkit" || die
		doins "${S}/tools/_polkit_auth" || die
		doins "${S}/tools/_polkit_action" || die
	fi

	# Need to keep a few directories around...
	keepdir /var/run/PolicyKit
	keepdir /var/lib/PolicyKit
}
